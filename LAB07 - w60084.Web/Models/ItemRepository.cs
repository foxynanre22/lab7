﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace LAB07___w60084.Models
{
    public class ItemRepository : IItemRepository
    {
        private static ConcurrentDictionary<string, Item> items =
            new ConcurrentDictionary<string, Item>();

        public ItemRepository()
        {
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Aaradnya", Surname = "Kharti", Age = 16, Gender = "Male" });
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Mirai", Surname = "Ahuja", Age = 22, Gender = "Female" });
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Zara", Surname = "Anand", Age = 16, Gender = "Male" });
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Viti", Surname = "Laghari", Age = 21, Gender = "Female" });
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Shrishti", Surname = "Patel", Age = 19, Gender = "Female" });
            Add(new Item { Id = Guid.NewGuid().ToString(), Name = "Atiksh", Surname = "Reddy", Age = 18, Gender = "Male" });
        }

        public IEnumerable<Item> GetAll()
        {
            return items.Values;
        }

        public void Add(Item item)
        {
            item.Id = Guid.NewGuid().ToString();
            items[item.Id] = item;
        }

        public Item Get(string id)
        {
            items.TryGetValue(id, out Item item);
            return item;
        }

        public Item Remove(string id)
        {
            items.TryRemove(id, out Item item);
            return item;
        }

        public void Update(Item item)
        {
            items[item.Id] = item;
        }
    }
}
