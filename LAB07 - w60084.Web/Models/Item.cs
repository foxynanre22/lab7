﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LAB07___w60084.Models
{
    public class Item
    {
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public int Age { get; set; }
        [Required]
        public string Gender { get; set; }
    }
}
