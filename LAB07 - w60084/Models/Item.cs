﻿using System;

namespace LAB07___w60084.Models
{
    public class Item
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public string Gender { get; set; }
    }
}