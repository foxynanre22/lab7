﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LAB07___w60084.Models;

namespace LAB07___w60084.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        readonly List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(), Name = "Aaradnya", Surname = "Kharti", Age = 16, Gender = "Male"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "Mirai", Surname = "Ahuja", Age = 22, Gender = "Female"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "Zara", Surname = "Anand", Age = 16, Gender = "Male"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "Viti", Surname = "Laghari", Age = 21, Gender = "Female"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "Shrishti", Surname = "Patel", Age = 19, Gender = "Female"},
                new Item { Id = Guid.NewGuid().ToString(), Name = "Atiksh", Surname = "Reddy", Age = 18, Gender = "Male"}
            };
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}